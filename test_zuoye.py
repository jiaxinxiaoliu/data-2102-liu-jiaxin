import pytest

def add(num1, num2):
    """
    计算两个数的加法结果
    :param num1: 整型或浮点型，范围为[-99, 99]
    :param num2: 整型或浮点型，范围为[-99, 99]
    :return: 整型或浮点型，加法结果
    """
    if not (-99 <= num1 <= 99) or not (-99 <= num2 <= 99):
        raise ValueError("Invalid input data, only numbers between -99 and 99 are allowed.")
    if isinstance(num1, float) or isinstance(num2, float):
        return round(num1 + num2, 2)
    else:
        return num1 + num2


@pytest.fixture(scope="function", autouse=True)
def print_start_end():
    print("开始计算")
    yield
    print("结束计算")


@pytest.mark.hebeu
@pytest.mark.parametrize("num1, num2, expected", [
    (10, 20, 30),
    (-10, -20, -30),
    (0, 0, 0),
    (99, 0, 99),
    (-99, 0, -99),
    (2.5, 3.5, 6),
    (-2.5, -3.5, -6),
    (2.234, 3.456, 5.69),
    (-2.234, -3.456, -5.69),
    (99, 0.01, 99.01),
    (-99, -0.01, -99.01),
])
def test_add_valid_inputs(num1, num2, expected):
    assert add(num1, num2) == expected


@pytest.mark.hebeu
@pytest.mark.parametrize("num1, num2", [
    (100, 50),
    (99, 100),
    (-100, -50),
    (-99, -100),
    (101, 102)
])
def test_add_invalid_inputs(num1, num2):
    with pytest.raises(ValueError):
        assert add(num1, num2)


